//Dependencies
var express = require('express');
var bodyParser = require('body-parser');

var fs = require('fs');
var http = require('http');
var url = require('url');
var path = require('path');

// set the port
var port = process.env.PORT || 9001;        

var dir = path.dirname(fs.realpathSync(__filename));

//Express
var app = express();

app.use(bodyParser.urlencoded({ extended: true })); // for parsing 
app.use(bodyParser.json()); // for parsing application/json
app.use("/data", express.static(__dirname + '/data')); //to serve static json data file
app.use("/js", express.static(__dirname + '/js')); //to serve static controller file

app.get('/', function(req, res) 
{
  var pathname = url.parse(req.url).pathname;
  var m;

    if (pathname == '/') 
    {
      res.writeHead(200, {'Content-Type': 'text/html'});
      //open file as readable stream and pipe it to the response object (client)
      fs.createReadStream(dir + '/payroll.html').pipe(res);
      return;
    }

    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.write('404 Not Found\n');
    res.end();
});

app.post('/data', function (req, res) {
  fs.readFile("./data/payrollData.json", function (err, data) {
    var jsonData = req.body;

    fs.writeFile("./data/payrollData.json", JSON.stringify(jsonData), function(err) {
      if(err) {
          return console.log(err);
      }
    });
  });

  res.end();
});

//Start server
app.listen(port, 'localhost');
console.log('Server running on port 9001');