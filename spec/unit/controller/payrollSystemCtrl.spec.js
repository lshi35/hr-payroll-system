'use strict';

describe('payrollSystemCtrl', function () {
    var scope, ctrl, $httpBackend, $filter;

    // load the controller's module
  	beforeEach(module('payrollSystem'));

    beforeEach(inject(function ($rootScope, $controller, _$httpBackend_, _$filter_) {
        $httpBackend = _$httpBackend_;       
        $filter = _$filter_;
        scope = $rootScope.$new();

        //initialise the controller
        ctrl = $controller('payrollSystemCtrl', { 
        	$scope: scope
        });       
    }));

    afterEach(function() {
	    $httpBackend.verifyNoOutstandingExpectation();
	    $httpBackend.verifyNoOutstandingRequest();
	});

    it('controller should be defined', function() {
    	expect(ctrl).toBeDefined();
    });

    it("should have a $scope variable", function() {
	   expect(scope).toBeDefined();
	});

    //$scope.date
    it('$scope.date should be defined as a date object', function() {		
    	expect(typeof(scope.date)).toBe('object');
    });

    // roundNumber function
    it('should round off number to closest integer', function() {
		var num = scope.roundNumber(9.4);			
    	expect(num).toBe(9);
    });

    //grossIncome function
    it('should call roundNumber method to calculate gross income', function() {
    	spyOn(scope, 'annualSal');
    	scope.grossIncome();
    	expect(scope.annualSal).toHaveBeenCalled();
    });

    it('return gross income earned for annual salary', function() {
    	spyOn(scope, 'annualSal').and.returnValue(60050);
    	var grossIncome = scope.grossIncome();
    	expect(grossIncome).toBe(5004.166666666667);
    });

    //incomeTax function
    it('should call annualSal method to calculate income tax', function() {
    	spyOn(scope, 'annualSal');
    	scope.incomeTax();
    	expect(scope.annualSal).toHaveBeenCalled();
    });

    it('should return income tax for annual salary earning', function() {
    	spyOn(scope, 'annualSal').and.returnValue(60050);
    	var incomeTax = scope.incomeTax();
    	expect(incomeTax).toBe(921.9375);
    });

    it('should return zero income tax for annual salary lesser than 18200', function() {
    	spyOn(scope, 'annualSal').and.returnValue(5000);
    	var incomeTax = scope.incomeTax();
    	expect(incomeTax).toBe(0);
    });

    //netIncome function
    it('should call grossIncome and incomeTax methods to calculate net income', function() {
    	spyOn(scope, 'grossIncome');
    	spyOn(scope, 'incomeTax');
    	scope.netIncome();
    	expect(scope.grossIncome).toHaveBeenCalled();
    	expect(scope.incomeTax).toHaveBeenCalled();
    });

    it('should return net income after taxes', function() {
    	spyOn(scope, 'grossIncome').and.returnValue(5004);
    	spyOn(scope, 'incomeTax').and.returnValue(922);
    	var netIncome = scope.netIncome();
    	expect(netIncome).toBe(4082);
    });

    //superAmt function
    it('should call grossIncome method to calculate super', function() {
    	spyOn(scope, 'grossIncome');
    	scope.superAmt();
    	expect(scope.grossIncome).toHaveBeenCalled();
    });

    //payAmt function
    it('should call netIncome and superAmt methods to calculate pay', function() {
    	spyOn(scope, 'netIncome');
    	spyOn(scope, 'grossIncome');
    	scope.payAmt();
    	expect(scope.netIncome).toHaveBeenCalled();
    	expect(scope.grossIncome).toHaveBeenCalled();
    });

    it('should return pay after taxes and super deducted', function() {
    	spyOn(scope, 'netIncome').and.returnValue(4082);
    	spyOn(scope, 'superAmt').and.returnValue(450);
    	var pay = scope.payAmt();
    	expect(pay).toBe(3632);
    });

    //generatePayslip function
	it('should call relevant methods to generate payslip', function() {
    	spyOn(scope, 'grossIncome');
    	spyOn(scope, 'netIncome');
    	spyOn(scope, 'incomeTax');
    	spyOn(scope, 'superAmt');
    	spyOn(scope, 'payAmt');
    	scope.generatePayslip();
    	expect(scope.grossIncome).toHaveBeenCalled();
    	expect(scope.netIncome).toHaveBeenCalled();
    	expect(scope.incomeTax).toHaveBeenCalled();
    	expect(scope.superAmt).toHaveBeenCalled();
    });   
});