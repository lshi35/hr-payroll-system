# **HR Payroll System** #

## Assumptions: ##
1. There can exist only one employee with a specific firstname and lastname.
2. When the user inputs employee details, the payslip generated will be for the current month.
3. The payslip information will be saved on the server only when the "Pay" button is hit
4. Pay date denotes the date on which the user pays the employee (aka current date).
5. Valid super rate range is between 0 -100.
6. Pay frequency is monthly for all employees.


## Limitations of the program: ##
1. The program keeps track of employee payment history for only one year.
2. The employee info is saved in a JSON file on the server. Ideally a database will need to be used. 


## How to run the program: ##
1. Git clone the repository.
2. Run npm install from project directory to install dependencies required.
3. Run npm start to start the server  
4. Access the program at http://localhost:9001/


## Reasons for choosing various frameworks: ##

### 1. AngularJS for Frontend ###
- The framework allows to create Single Page Applications easily and enforces the MVC design pattern.
- It makes a clear separation between the frontend and backend which helps to keep the code clean and maintainable.
- Angular allows to easily run unit tests on the application using Jasmine.


### 2. Jasmine for unit testing ###
- Jasmine framework was used because it supports testing AngularJS code, on which the program is built.
- Jasmine allows to structure tests and make assertions easily. Moreover its the popular tool used for testing AngularJS applications.