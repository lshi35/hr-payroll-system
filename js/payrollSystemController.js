var app = angular.module('payrollSystem', []);

app.controller('payrollSystemCtrl', PayrollSystemCtrl);

function PayrollSystemCtrl($scope, $http, $filter) 
{
	var currentEmp = {};

	$scope.view = "";
	$scope.date = new Date();

	//a convenient list of exposed methods
	$scope.employeeInfo = employeeInfo;
	$scope.roundNumber = roundNumber;
	$scope.payDate = payDate;
	$scope.annualSal = annualSal;
	$scope.grossIncome = grossIncome;
	$scope.incomeTax = incomeTax;
	$scope.netIncome = netIncome;
	$scope.superAmt = superAmt;
	$scope.payAmt = payAmt;
	$scope.generatePayslip = generatePayslip;
	$scope.payEmployee = payEmployee;

	//initial view
	setView('employeeInfo');
	
	//display view requested
	function setView(view)
	{ 
		$scope.view = view;
	}

	//Returns view to Employee view form when cancel button is pressed on the payslip generated
	function employeeInfo() {
		setView('employeeInfo');
		currentEmp = {}; //reset object
	}

	function roundNumber(num) 
	{
		return Math.round(num); 
	}

	//returns current date in format "dd Month year"
	function payDate() {
		return $filter('date')($scope.date, "dd MMMM yyyy");
	}

	//returns annual salary based on user input
	function annualSal() {
		return currentEmp.annualSal;
	}

	//returns monthly income rounded off to nearest whole number
	function grossIncome() 
	{
		return $scope.annualSal()/12;
	}

	//returns income tax payable for the income
	function incomeTax() 
	{
		var income = $scope.annualSal();

		// income between 0 - $18200
		if(income >= 0 && income <= 18200) {
			return 0;
		}

		// income between $18201 - $37000
		else if(income >= 18201 && income <= 37000) {
			var taxableIncome = income - 18200;
			return (0.19*taxableIncome)/12;
		}

		// income between $37001 = $80000
		else if(income >= 37001 && income <= 80000) {
			var taxableIncome = income - 37000;
			return ((0.325*taxableIncome) + 3572)/12;
		}

		// income between $80001 - $180000
		else if(income >= 80001 && income <= 180000) {
			var taxableIncome = income - 80000;
			return ((0.37*taxableIncome) + 17547)/12;
		}

		//income greater than $180001
		else if(income >= 180001) {
			var taxableIncome = income - 180000;
			return ((0.45*taxableIncome) + 54547)/12;
		}
	}

	//returns income received after taxes
	function netIncome() 
	{
		return $scope.grossIncome() - $scope.incomeTax();
	}

	//returns the super amount received for income - calculated based on the super rate
	function superAmt() 
	{
		return $scope.grossIncome() * (currentEmp.superRate/100);
	}

	//returns amount to be paid to employee
	function payAmt() 
	{
		return $scope.netIncome() - $scope.superAmt();
	}

	//Generate payslip
	function generatePayslip()
	{	
		//store employee details in a temp javascript object
		currentEmp.fname = $scope.fnameInput;
		currentEmp.lname = $scope.lnameInput;
		currentEmp.annualSal = $scope.annualSalInput;
		currentEmp.superRate = $scope.superRateInput;
		currentEmp.grossIncome = roundNumber(grossIncome());
		currentEmp.incomeTax = roundNumber(incomeTax());
		currentEmp.netIncome = roundNumber(netIncome());
		currentEmp.superAmt = roundNumber(superAmt());
		currentEmp.payAmt = roundNumber(payAmt());
		/*
			Array that stores info on employee payments, 
				indices represent the month & 
				values represent whether employee has been paid or not 
					false - not paid for the month yet
					string value - date string which denotes when employee was paid
		*/
		currentEmp.payDates = [false,false,false,false,false,false,false,false,false,false,false,false]; 
		
		setView('payslip');
	}

	/* 
		Sends employee info to server based on whether employee has been paid
		If paid - return alert to user
		Else - send pay info for the month, to be stored in server 
	*/
	function payEmployee() {
	    $http.get('data/payrollData.json')
		    .success(function(data) {
		    	//Check whether employee already exists
		    	//Assumption: There can exist only one user with a particular firstname & lastname
				var found = data.findIndex(function (el) 
				{	
    				return (el.fname.toLowerCase() === currentEmp.fname.toLowerCase() && 
    				el.lname.toLowerCase() === currentEmp.lname.toLowerCase()); //ignorecase 
    			});

				//Employee not found on server
    			if(found == -1) {
    				//Store current date string for the relevant month in the array
    				var month = $filter('date')($scope.date, "MM");
					currentEmp.payDates[month-1] = ($scope.payDate());

					var newJson = data;
					newJson.push(currentEmp); //add new employee info to json data on server

					//Send updated emp data to server
    				$http.post('/data', newJson)
						.success(function(data) {
							alert("Employee has been paid successfully!");
						})
						.error(function(data) {
							alert("Error: Employee couldn't be paid!");
							console.error("Error in posting data: " + data);
						});
    			}

    			//Employee found on server 
    			else {
    				var month = $filter('date')($scope.date, "MM");
    				
    				//employee not paid for the month
    				if(data[found].payDates[month-1] == false) {
    					currentEmp.payDates = data[found].payDates; //set payDates array to reflect emp payment history
    					currentEmp.payDates[month-1] = ($scope.payDate());
    					
    					var newJson = data;
    					//remove old emp data n replace it with updated info
						newJson.splice(found,1,currentEmp); //TODO very inefficient strategy, use http.put to update server data instead

						//Send updated data to server
    					$http.post('/data', newJson)
						.success(function(data) {
							alert("Employee has been paid successfully!");
						})
						.error(function(data) {
							alert("Error: Employee couldn't be paid!");
							console.error("Error in posting data: " + data);
						});

    				}

    				//Employee already paid for the month
    				else {
    					alert("Employee has already been paid for this month"); //TODO beautify alert box display
    				}
    			}
		    })
		    .error(function(data) {
				console.error("Error in fetching feed: " + data);
			});  
	}
}